package ist.challenge.ryan_anggriawan.controller;

import ist.challenge.ryan_anggriawan.model.UsersModel;
import ist.challenge.ryan_anggriawan.service.UsersService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UsersController {

    private final UsersService usersService;

    public UsersController(UsersService usersService){
        this.usersService = usersService;
    }

    @GetMapping("/register")
    public String getRegisterPage(Model model){
        model.addAttribute("registerRequest", new UsersModel());
        return "register_page";
    }

    @GetMapping("/login")
    public String getLoginPage(Model model){
        model.addAttribute("loginRequest", new UsersModel());
        return "login_page";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute UsersModel usersModel){
        System.out.println("Register request: " + usersModel);
        UsersModel registeredUser = usersService.registerUser(usersModel.getUsername(), usersModel.getPassword());
        return registeredUser == null ? "error_page" : "redirect:/login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute UsersModel usersModel, Model model){
        System.out.println("Login request: " + usersModel);
        UsersModel authenticated = usersService.authenticate(usersModel.getUsername(), usersModel.getPassword());
        if(authenticated != null){
            model.addAttribute("users", usersService.getAllUsers());
            return "main_page";
        } else {
            return "error_page";
        }
    }

    @GetMapping("/user/edit/{id}")
    public String editUserForm(@PathVariable Long id, Model model){
        model.addAttribute("user", usersService.getUserById(id));
        return "edit_page";
    }

    @PostMapping("/user/{id}")
    public String updateUser(@PathVariable Long id, @ModelAttribute UsersModel usersModel, Model model){
        UsersModel existingUser = usersService.getUserById(id);
        existingUser.setId(usersModel.getId());
        existingUser.setUsername(usersModel.getUsername());
        existingUser.setPassword(usersModel.getPassword());
        usersService.updateUser(existingUser);
        model.addAttribute("users", usersService.getAllUsers());
        return "main_page";
    }
}
