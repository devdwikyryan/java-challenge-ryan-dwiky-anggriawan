package ist.challenge.ryan_anggriawan.repository;

import ist.challenge.ryan_anggriawan.model.UsersModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<UsersModel, Long> {

    Optional<UsersModel> findByUsernameAndPassword(String username, String password);

    Optional<UsersModel> findFirstByUsername(String username);

//    List<UsersModel> findAll();
}
