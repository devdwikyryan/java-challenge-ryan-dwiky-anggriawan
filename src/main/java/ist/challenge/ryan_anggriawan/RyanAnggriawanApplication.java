package ist.challenge.ryan_anggriawan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RyanAnggriawanApplication {

	public static void main(String[] args) {
		SpringApplication.run(RyanAnggriawanApplication.class, args);
	}

}
