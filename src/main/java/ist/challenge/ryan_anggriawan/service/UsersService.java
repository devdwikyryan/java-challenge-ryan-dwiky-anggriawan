package ist.challenge.ryan_anggriawan.service;

import ist.challenge.ryan_anggriawan.model.UsersModel;
import ist.challenge.ryan_anggriawan.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository){
        this.usersRepository = usersRepository;
    }

    public UsersModel registerUser(String username, String password){
        if(username == null || password == null){
            return null;
        } else {
            if(usersRepository.findFirstByUsername(username).isPresent()){
                System.out.println("Duplicated login");
                return null;
            }
            UsersModel usersModel = new UsersModel();
            usersModel.setUsername(username);
            usersModel.setPassword(password);
            return usersRepository.save(usersModel);
        }
    }

    public UsersModel authenticate(String username, String password){
        return usersRepository.findByUsernameAndPassword(username, password).orElse(null);
    }

    public List<UsersModel> getAllUsers(){
        System.out.println("Result: " + usersRepository.findAll());
        return usersRepository.findAll();
    }

    public UsersModel getUserById(Long id){
        return usersRepository.findById(id).get();
    }

    public UsersModel getUserByUsername(String username){
        return usersRepository.findFirstByUsername(username).get();
    }

    public UsersModel updateUser(UsersModel usersModel){
        return usersRepository.save(usersModel);
    }
}

